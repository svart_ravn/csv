-module(csv).

-export([
	parse/1,
	parse/2
]).


-define(BUFFER_SIZE, 1024).

-record(csv_options, {
	column_separator, 
	row_separator, 
	column_enclosed = empty
}).


-record(parse_options, {
	state = simple, %% simple|enclosing
	column_value,
	tuple,
	list
}).

%% ========================================================
%%                       API
%% ========================================================

parse(FileName) ->
	parse(FileName, #csv_options{column_separator=$\t, row_separator=$\n, column_enclosed=$\"}).


parse(FileName, ReadOptions) ->
	{ok, Device} = file:open(FileName, [read]),
	NewPO = read_data_from_file(Device, ReadOptions, empty_parse_options()),
	NewPO#parse_options.list.


%% ========================================================
%%                       Internals
%% ========================================================

empty_parse_options() ->
	#parse_options{state = simple, column_value = "", tuple = {}, list = []}.


read_data_from_file(Device, ReadOptions, ParseOptions)	->
    case file:read(Device, ?BUFFER_SIZE) of
        eof  -> 
        	file:close(Device),
        	tuple_completed(ParseOptions);
        {ok, Data} -> 
        	NewPO = extract_data(Data, ReadOptions, ParseOptions),
        	read_data_from_file(Device, ReadOptions, NewPO)
    end.


extract_data([], _ReadOptions, ParseOptions) ->
	ParseOptions;

extract_data([H|T], ReadOptions, ParseOptions) ->
	NewPS = choose_action(H, ReadOptions, ParseOptions),
	extract_data(T, ReadOptions, NewPS).


choose_action(ColumnEnclosed, #csv_options{column_enclosed = ColumnEnclosed}, PO) when is_atom(ColumnEnclosed) ->
	skip_char(PO);

choose_action(ColumnEnclosed, #csv_options{column_enclosed = ColumnEnclosed}, PO) when is_integer(ColumnEnclosed) ->
	do_enclosing_action(ColumnEnclosed, PO, enclosing);

choose_action(RowSeparator, #csv_options{row_separator = RowSeparator}, PO = #parse_options{state = simple}) ->
	tuple_completed(PO);

choose_action(ColSeparator, #csv_options{column_separator = ColSeparator}, PO = #parse_options{state = simple}) ->
	column_completed(PO);

choose_action(Char, _Options, PO) ->
	add_char(Char, PO).


do_enclosing_action(_Char, PO = #parse_options{state = enclosing}, enclosing) ->
	skip_char(PO#parse_options{state = simple});

do_enclosing_action(_Char, PO = #parse_options{state = simple}, enclosing) ->
	skip_char(PO#parse_options{state = enclosing}).


skip_char(PO) ->
	PO.


add_char(Char, PO = #parse_options{column_value = Value}) ->
	PO#parse_options{column_value = Value ++ [Char]}.


column_completed(PO = #parse_options{tuple = Tuple, column_value = Value}) ->
	PO#parse_options{column_value = "", tuple = erlang:append_element(Tuple, Value)}.


tuple_completed(PO) ->
	NewPS = complete_nonempty_column(PO, length(PO#parse_options.column_value)),
	NewPS#parse_options{tuple = {}, list = NewPS#parse_options.list ++ [NewPS#parse_options.tuple]}.


complete_nonempty_column(PO, 0) ->
	PO;

complete_nonempty_column(PO, _Len) ->
	column_completed(PO).
